//jQuery to collapse the navbar on scroll
$(window).scroll(function() {
    if ($(".navbar").offset().top > 50) {
        $(".navbar-fixed-top").addClass("top-nav-collapse");
    } else {
        $(".navbar-fixed-top").removeClass("top-nav-collapse");
    }
});

//jQuery for page scrolling feature - requires jQuery Easing plugin
$(function() {
    $('.page-scroll a').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });

    $(".navbar-toggle, .lgvt-nav-menu-list-item a")
        .bind('click', function(e){
        $(".lgvt-nav-menu").toggleClass('collapse');
    });

    function toggleDialog(content){
        $(".lgvt-dialog").toggleClass('collapse');
        if(content){
          $(".lgvt-dialog-content")
              .find(".lgvt-dialog-content-contents")
              .each(function(el){
                  $(this).addClass('collapse');
              });
          $(".lgvt-dialog-content")
              .find("#"+content)
              .toggleClass('collapse');
        }
    }

    $("#lgvt-dialog-close-button").bind('click', function(){
        toggleDialog();
    });

    $('.lgvt-text-container-dialog a').bind('click', function(){
        var content = $(this).attr('data-content');
        toggleDialog(content);
    });


    var LGVTSwiper = $('.swiper-container').swiper({
        slidesPerView: 'auto'
    });

    $('.arrow-left').on('click', function(e){
        e.preventDefault();
        LGVTSwiper.swipePrev();
    });

    $('.arrow-right').on('click', function(e){
        e.preventDefault();
        LGVTSwiper.swipeNext();
    });
});


